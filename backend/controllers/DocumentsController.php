<?php

class DocumentsController extends Controller {

    /** @var FileController */
    private $fileController = null;

    function generatePreviewAction() {
        $this->fileController = new FileController(false);
        if(!$this->auth->getLoggedUserId()) throw new Exception("You must be logged in to generate document!");
        if(empty($_POST['id'])) throw new Exception("Empty ID");
        $docId = $_POST['id'];
        $document = $this->store->findById('documents', $docId);
        if(empty($document)) throw new Exception("Document not exists #" . $docId);
        $htmlPath = $this->fileController->filePath('document', 'html', $docId);
        $pngPath = $this->fileController->filePath('document', 'png', $docId);
        $this->saveHtml($document['blocks'], $htmlPath);
        $this->generatePreview($htmlPath, $pngPath);
        $this->printOk();
    }

    function downloadAction() {
        $path = $this->generateDocument($_POST['blocks']);
        $pdfPath = $path .'.pdf';

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=Document '.date("d-m-Y").'.pdf');
        header('Content-Transfer-Encoding: binary');
        //header('Expires: 0');
        //header('Cache-Control: must-revalidate');
        //header('Pragma: public');
        header('Content-Length: ' . filesize($pdfPath));
        readfile($pdfPath);

        $this->cleanUp($path);
    }

    function printAction() {
        $path = $this->generateDocument($_POST['blocks']);
        $pdfPath = $path .'.pdf';
        $this->fileController->sendHeader($pdfPath);
        readfile($pdfPath);
        $this->cleanUp($path);
    }

    function generateDocument($blocks) {
        $this->fileController = new FileController(false);
        if(!$this->auth->getLoggedUserId()) throw new Exception("You must be logged in to print document!");

        $tmpPath = '../backend/files/tmp/';
        $tmpFileName = md5(rand(1000, 9999999));
        $htmlPath = $tmpPath . $tmpFileName . '.html';
        $pdfPath = $tmpPath . $tmpFileName . '.pdf';
        $this->saveHtml($blocks, $htmlPath, true);
        $this->generatePdf($htmlPath, $pdfPath);

        return $tmpPath . $tmpFileName;
    }

    function cleanUp($path) {
        @unlink($path . '.html');
        @unlink($path . '.pdf');
    }

    function generatePdf($htmlPath, $pdfPath) {
        exec('wkhtmltopdf -q ' . $htmlPath . ' ' . $pdfPath);
    }

    function generatePreview($htmlPath, $pngPath) {
        exec('wkhtmltoimage -q ' . $htmlPath . ' ' . $pngPath);
        exec('convert -resize 330 -crop 330x330 -extent 330x330 ' . $pngPath .' ' . $pngPath);
    }

    private function saveHtml($blocks, $htmlPath, $appendFooter=false) {
        $html = '';
        foreach($blocks AS $blockId) {
            $block = $this->store->findById('blocks', $blockId);
            if(empty($block)) throw new Exception("Block " . $blockId ." not found!");
            $html .= $block['value'];
        }
        if($appendFooter) {
            $html .= file_get_contents($this->fileController->filePath('pattern', 'html', 'footer'));
        }
        $layout = file_get_contents($this->fileController->filePath('pattern', 'html', 'layout'));
        file_put_contents($htmlPath, str_replace('{{outlet}}', $html, $layout));
    }
}