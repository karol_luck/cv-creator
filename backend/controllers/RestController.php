<?php

class RestController extends Controller {
    function getAction() {
        $model = $_GET['model'];
        $select = $_GET['select'];

        if($select == 'single') {
            $collection = $model;
            $found = $this->store->findById($collection, $_GET['id']);
            $this->printJson($found, $model);
        }
        else {
            $collection = $model;
            $findParams = $_GET;
            unset($findParams['controller']);
            unset($findParams['method']);
            unset($findParams['model']);
            unset($findParams['select']);
            $found = $this->store->find($collection, $findParams);
            $this->printJson($found, $model);
        }
    }
    function putAction() {
        $this->checkLoggedIn();
        $collection = $_GET['model'];
        $id = $_GET['id'];
        $payload = $this->getPayload();
        $data = $payload[$this->toSingular($collection)];

        $updatedData = $this->store->update($collection, $id, $data);
        $updatedData['_id'] = $id;
        $this->printJson($updatedData, $this->toSingular($collection));
    }
    function postAction() {
        $this->checkLoggedIn();
        $collection = $_GET['model'];
        $payload = $this->getPayload();
        $data = $payload[$this->toSingular($collection)];

        $savedData = $this->store->create($collection, $data);
        $this->printJson($savedData, $this->toSingular($collection));
        //$savedData
    }
    function deleteAction() {
        $this->checkLoggedIn();
        $collection = $_GET['model'];
        $id = $_GET['id'];
        $this->store->delete($collection, $id);
        $this->printNoContent();
    }

    private function checkLoggedIn() {
        if(!$this->auth->getLoggedUserId()) {
            throw new Exception("Cannot do any put/post/delete REST action if not logged in");
        }
    }
}