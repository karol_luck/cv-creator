<?php

class BlocksController extends Controller {

    private $patternTagRegex = "/{{([a-z0-9]*)}}/";
    private $outletTag = "{{outlet}}";
    private $userData = array();
    private $blockData = array();
    private $currentData;
    /** @var FileController */
    private $fileController;

    function generateAction() {
        if(!$this->auth->getLoggedUserId()) throw new Exception("You must be logged in to generate blocks!");

        $this->userData = $this->store->findById('users', $this->auth->getLoggedUserId());
        $type = $_POST['type'];
        $this->fileController = new FileController(false);
        $patternPath = $this->fileController->filePath('pattern', 'html', $type);
        $patternContent = file_get_contents($patternPath);
        if($type == 'custom') {
            $this->blockData = $this->store->findById('blocks', $_POST['id']);
            if(!empty($this->blockData['title'])) $this->blockData['title'] = '<h2>'.$this->blockData['title'].'</h2>';
            $type = 'custom.' . $_POST['id'];
        }
        $html = preg_replace_callback($this->patternTagRegex, array($this, 'fillTag'), $patternContent);
        $this->saveBlock($type, $html);
        if($_POST['type'] == 'custom') {
            $this->updateCustomBlock($_POST['id'], $html);
        }
        else {
            $this->saveToDb($type, $html);
        }
        $this->generatePreview($type);
        $this->printOk();
        //echo 'done: ' . preg_replace_callback($this->patternTagRegex, array($this, 'fillTag'), $patternContent);
    }

    private function generatePreview($type) {
        $htmlPath = $this->fileController->filePath('block', $type.'.preview.html', $this->userData['_id']);
        $pngPath = $this->fileController->filePath('block', $type.'.preview.png', $this->userData['_id']);
        exec('wkhtmltoimage -q ' . $htmlPath . ' ' . $pngPath);
        exec('convert -resize 500 ' . $pngPath .' ' . $pngPath);
    }

    private function saveBlock($type, $html) {
        $userId = $this->auth->getLoggedUserId();
        if(empty($userId)) throw new Exception("You must be logged in to save blocks #1!");
        $layout = file_get_contents($this->fileController->filePath('pattern', 'html', 'layout'));
        $previewHtml = str_replace($this->outletTag, $html, $layout);
        // not necesarry since content is saving to store
        //file_put_contents($this->fileController->filePath('block', $type.'.html', $userId), $html);
        file_put_contents($this->fileController->filePath('block', $type.'.preview.html', $userId), $previewHtml);
    }

    private function updateCustomBlock($id, $html) {
        $userId = $this->auth->getLoggedUserId();
        if(empty($userId)) throw new Exception("You must be logged in to save blocks #3!");
        $savedBlock = $this->store->findOne('blocks', array('_id' => new MongoId($id), 'type' => 'custom', 'user' => $userId));
        if(empty($savedBlock)) throw new Exception("Block #" . $id ." / 'custom' / ". $userId. " not found!");
        $savedBlock['value'] = $html;
        $this->store->update('blocks', $id, $savedBlock);
    }

    private function saveToDb($type, $html, $title = null) {
        $userId = $this->auth->getLoggedUserId();
        if(empty($userId)) throw new Exception("You must be logged in to save blocks #2!");
        $savedBlock = $this->store->findOne('blocks', array('type' => $type, 'user' => $userId));
        $blockData = array(
            'user' => $userId,
            'type' => $type,
            'title' => $title,
            'value' => $html
        );
        if(empty($savedBlock)) {
            $this->store->create('blocks', $blockData);
        }
        else {
            $this->store->update('blocks', $savedBlock['_id'], $blockData);
        }
    }

    private function fillTag($found) {
        $tag = $found[1];
        if(!empty($this->blockData) && array_key_exists($tag, $this->blockData)) {
            $val = $this->blockData[$tag];
        }
        else if(array_key_exists($tag, $this->userData)) {
            $val = $this->userData[$tag];
        }
        else {
            return $this->transform($tag, $found[0]);
        }

        if(is_array($val)) {
            $this->sectionData = $val;
            $value = '';
            $eachPath = '../backend/files/patterns/each/' . $tag . '.html';
            if(!file_exists($eachPath)) return $found[0];
            $eachPattern = file_get_contents($eachPath);
            foreach($val AS $data) {
                $this->currentData = $data;
                $value .= preg_replace_callback($this->patternTagRegex, array($this, 'fillEach'), $eachPattern);
            }
            return $this->transform($tag, $value);
        }
        else {
            return $this->transform($tag, $val);
        }
    }

    private function transform($tag, $val) {
        if($tag == 'avatar') {
            $userAvatar = 'avatars/'.$this->userData['_id'].'.png';
            return file_exists('../backend/files/'.$userAvatar) ? '../'.$userAvatar : '../default/avatar.png';
        }
        else if($tag == 'end') {
            if(empty($val) || $val == '{{' . $tag . '}}') return '';
            else return ' - ' . $val;
        }
        return $val;
    }

    private function fillEach($found) {
        $tag = $found[1];
        if(!is_array($this->currentData)) {
            if($tag != 'value') return $this->transform($tag, $found[0]);
            return $this->transform($tag, $this->currentData);
        }
        if(!array_key_exists($tag, $this->currentData)) { return $this->transform($tag, $found[0]); }
        return $this->transform($tag, $this->currentData[$tag]);
    }
}