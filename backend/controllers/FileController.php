<?php

class FileController extends Controller {

    function avatarDeleteAction() {
        $userId = $this->auth->getLoggedUserId();
        if(empty($userId)) {
            $this->printError('You are not logged in!');
            return;
        }
        $this->deleteFile('avatar', $userId, 'png');
        $this->printOk();
    }

    function avatarImageAction() {
        if(empty($_GET['userId'])) {
            throw new Exception("Empty userId (" . $_GET['userId'] .")");
        }
        $this->validate($_GET['userId'], 'userId');
        $this->getFile('avatar', $_GET['userId'], 'png');
    }

    function avatarUploadAction() {
        $userId = $this->auth->getLoggedUserId();
        if(empty($userId)) {
            $this->printError('You are not logged in!');
            return;
        }
        if(empty($_FILES) || empty($_FILES['avatarImage'])) {
            $this->printError('No files sent');
            return;
        }
        $file = $_FILES['avatarImage'];
        if($file['type'] != 'image/png') {
            $this->printError('Only png files are accepted as avatars');
            return;
        }
        $this->acceptFile($file, 'avatar', $userId, 'png');
        $this->printOk();
    }

    function blockAction() {
        $userId = $this->auth->getLoggedUserId();
        $type = $_GET['type'];
        if(empty($userId)) {
            $this->printError('You are not logged in!');
            return;
        }
        if(empty($type)) {
            $this->printError('Type not set!');
            return;
        }
        $this->validate($type, 'type');
        if($type == 'custom') {
            $this->validate($_GET['id'], 'block id');
            $type = 'custom.' . $_GET['id'];
        }
        $this->getFile('block', $userId, $type . '.preview.png');
    }

    function documentPreviewAction() {
        $userId = $this->auth->getLoggedUserId();
        $docId = $_GET['id'];
        if(empty($userId)) {
            $this->printError('You are not logged in!');
            return;
        }
        if(empty($docId)) {
            $this->printError('Type not set!');
            return;
        }
        $this->validate($docId, 'id');
        $this->getFile('document', $docId, 'png');
    }

    protected function acceptFile($file, $type, $name, $format) {
        move_uploaded_file($file['tmp_name'], $this->filePath($type, $format, $name));
    }

    protected function deleteFile($type, $name, $format) {
        $filePath = $this->filePath($type, $format, $name);
        if(file_exists($filePath)) {
            unlink($this->filePath($type, $format, $name));
        }
    }

    protected function getFile($type, $name, $format) {
        $filePath = $this->filePath($type, $format, $name);
        $defaultFilePath = $this->filePath($type, $format);
        if(file_exists($filePath)) {
            $loadPath = $filePath;
        }
        else if(file_exists($defaultFilePath)) {
            $loadPath = $defaultFilePath;
        }
        else {
            throw new Exception("Bad format of " . $type ." (" . $format .") OR default file not exists");
        }
        $this->sendHeader($loadPath);
        readfile($loadPath);
    }

    public function filePath($type, $format, $name = false) {
        $this->validate($type, 'type');
        $this->validate($format, 'format');
        if($name === false) {
            return '../backend/files/default/' . $type . '.' . $format;
        }
        $this->validate($name, 'name');
        return '../backend/files/' . $this->toPlural($type) . '/'. $name.'.'.$format;
    }

    function sendHeader($filePath) {
        $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($fileInfo, $filePath);
        header("Content-type: " . $mimeType);
    }

    private function validate($variable, $name = null) {
        if(!$name) $name = 'variable';
        $regex = '/^[a-zA-z0-9_\.]*$/';
        if(!preg_match($regex, $variable)) throw new Exception("Invalid $name ($variable)");
    }

}