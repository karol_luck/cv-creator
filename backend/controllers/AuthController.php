<?php

class AuthController extends Controller {
    /** @var Auth */
    protected $auth;

    function loginAction() {
        $this->auth->login($_POST['email'], $_POST['password']);
        $this->loggedUserIdAction();
    }

    function logoutAction() {
        $this->auth->logout();
        $this->printOk();
    }

    function loggedUserIdAction() {
        $userId = $this->auth->getLoggedUserId();
        $this->printJson(array('id' => $userId));
    }

    function changePasswordAction() {
        if(empty($_POST['newPassword'])||strlen($_POST['newPassword']) <= 3) throw new Exception('Empty newPassword');

        $this->store->update('users', $this->auth->getLoggedUserId(),
            array('password' => $this->auth->hashPwd($_POST['newPassword']))
        );
        $this->printOk();
    }
}