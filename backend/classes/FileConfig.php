<?php

class FileConfig implements IConfig {
    private $loadedConfig = array();

    function __construct($name) {
        include('../backend/config/'.$name.'.php');
        $this->loadedConfig = $config;
    }

    function get($key) {
        if(!array_key_exists($key, $this->loadedConfig)) return null;
        return $this->loadedConfig[$key];
    }
}