<?php

abstract class Controller {
    /** @var IStore */
    protected $store;

    /** @var Auth */
    protected $auth;

    /** @var IConfig */
    protected $config;

    function __construct($initVars = true) {
        if($initVars) {
            $this->config = new FileConfig('config');
            $this->store = new MongoStore($this->config);
            $this->auth = new Auth($this->store);
        }
    }

    function printOk() {
        $this->printJson(array('status' => 'ok'));
    }

    function printError($error) {
        $this->printJson(array('status' => 'error', 'message' => $error));
    }

    function printNoContent() {
        header("HTTP/1.0 204 No Content");
    }

    function printJson($data, $modelName = null) {
        if($modelName) {
            $data = array($modelName => $data);
        }
        $json = json_encode($data);
        echo $json;
    }

    protected function toPlural($singular) {
        if(substr($singular, 0, -1) == 'y') {
            return substr($singular, -1) . 'ies';
        }
        return $singular . 's';
    }

    protected function toSingular($plural) {
        if(substr($plural, 0, -3) == 'ies') {
            return substr($plural, -3) . 'y';
        }
        return substr($plural, 0, -1);
    }

    protected function getPayload() {
        return json_decode(file_get_contents('php://input'), true);
    }
}