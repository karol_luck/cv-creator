<?php

abstract class Model {
    protected $modelName;

    function __construct() {
        $this->modelName = get_class($this);
    }
}