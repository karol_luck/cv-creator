<?php

class MongoStore implements IStore {
    private $connection;
    private $db;

    /**
     * @param IConfig $config
     */
    function __construct($config) {
        $this->connection = new MongoClient($config->get('db.url'));
        $this->db = $this->connection->selectDB($config->get('db.name'));
    }

    function create($name, $data) {
        $this->getCollection($name)->insert($data);
        $data['_id'] = (string)$data['_id'];
        return $data;
    }

    function find($name, $data) {
        $this->validateData($data);
        $found = array();
        $cursor = $this->getCollection($name)->find($data);
        while($recordData = $cursor->getNext()) {
            $this->parseId($recordData);
            $found[] = $recordData;
        }
        return $found;
    }

    function findOne($name, $data) {
        $this->validateData($data);
        $recordData = $this->getCollection($name)->findOne($data);
        $this->parseId($recordData);
        return $recordData;
    }

    function findById($name, $id) {
        return $this->findOne($name, array('_id' => new MongoId($id)));
    }

    function update($name, $id, $data) {
        $storeData = $this->findById($name, $id);
        if(empty($storeData)) throw new Exception("Empty data ($name / $id)");
        $mergedData = array_merge($storeData, $data);
        unset($mergedData['_id']);
        $this->db->selectCollection($name)->update(array('_id' => new MongoId($id)), $mergedData);
        return $mergedData;
    }

    function delete($name, $id) {
        $this->db->selectCollection($name)->remove(array('_id' => new MongoId($id)));
        return true;
    }

    private function getCollection($collection) {
        return $this->db->selectCollection($collection);
    }

    private function parseId(&$data) {
        if(!$data['_id']) return;
        $data['_id'] = (string)$data['_id'];
    }

    private function validateData($data) {
        foreach($data AS $k=>$w) {
            if($w === null) throw new Exception("Bad value for key: ". $k ." ( ". json_encode($data) ." )");
        }
    }
}