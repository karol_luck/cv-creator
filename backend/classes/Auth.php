<?php

class Auth {

    /** @var IStore */
    protected $store;

    function __construct($store) {
        session_start();
        $this->store = $store;
    }

    function login($email, $password) {
        $found = $this->store->findOne('users', array('email' => $email));
        if($found && $found['password'] == $this->hashPwd($password)) {
            $this->saveToSession($found['_id']);
            return true;
        }
        $this->logout();
        return false;
    }

    function logout() {
        unset($_SESSION['userId']);
    }

    function getLoggedUserId() {
        return $this->loadFromSession();
    }

    private function saveToSession($id) {
        $_SESSION['userId'] = $id;
    }

    private function loadFromSession() {
        return empty($_SESSION['userId']) ? null : $_SESSION['userId'];
    }

    public function hashPwd($password) {
        return sha1($password);
    }
}