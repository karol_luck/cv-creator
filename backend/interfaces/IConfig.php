<?php

interface IConfig {
    /**
     * @param string $key
     * @return mixed
     */
    function get($key);
}