<?php

interface IStore {
    /**
     * @var string $name
     * @var array $data
     * @return void
     */
    function create($name, $data);

    /**
     * @param string $name
     * @param array $data
     * @return array
     */
    function find($name, $data);

    /**
     * @param string $name
     * @param array $data
     * @return array
     */
    function findOne($name, $data);

    /**
     * @param string $name
     * @param mixed $id
     * @return array
     */
    function findById($name, $id);

    /**
     * @param string $name
     * @param mixed $id
     * @param array $data
     * @return array
     */
    function update($name, $id, $data);

    /**
     * @param string $name
     * @param mixed $id
     * @return bool
     */
    function delete($name, $id);
}