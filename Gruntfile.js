var fs = require('fs');
var path = require('path');
var templateCompiler = require('./public/libs/ember/ember-template-compiler');

module.exports = function(grunt) {

    var templatesPath = 'src/templates/';

    grunt.conf = {
        banner: '/*\n  <%= pkg.name %> v<%= pkg.version %> \n  Generate date <%= grunt.template.today("yyyy-mm-dd, HH:MM:ss") %> \n*/\n',
        sources: [
            'src/**/*.js'
        ]
    };

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        compileHtmlBars: {
            build: {
                templateBasePath: templatesPath,
                src: [templatesPath + '**/*.hbs'],
                dest: 'public/js/templates.js'
            }
        },
        concat: {
            options: {
                banner: this.conf.banner
            },
            app: {
                src: this.conf.sources,
                dest: 'public/js/app.js'
            }
        },
        less: {
            options: {
                paths: ["public_html/css"]
            },
            build: {
                files: {
                    "public/css/app.css": "src/less/main.less"
                }
            }
        }
    });

    // Custom tasks definitions
    grunt.registerMultiTask('compileHtmlBars', 'Pre-compile HTMLBars tempates', function() {

        var done = this.async();

        this.files.forEach(function (file) {

            var stream = fs.createWriteStream(path.join(__dirname, file.dest), {
                encoding: 'utf8'
            });

            stream.once('open', function (fd) {
                grunt.log.writeln('Pre-compiling ' + file.src.length + ' handlebars templates...');

                // process each template
                file.src.forEach(function (f) {

                    // load the template
                    var template = fs.readFileSync(path.join(__dirname, f), {
                        encoding: 'utf8'
                    });
                    var name = f.replace(new RegExp('^' + file.templateBasePath), '').replace(/\.hbs$/, '');

                    // compile the template
                    stream.write('Ember.TEMPLATES["' + name + '"] = Ember.HTMLBars.template(');
                    stream.write(templateCompiler.precompile(template) + ');\n\n');
                });

                stream.end(done);
            });
        });
    });

    // Load npm tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');

    // Commands definitions
    grunt.registerTask('default', ['compileHtmlBars', 'concat', 'less']);
    grunt.registerTask('js', ['concat']);
    grunt.registerTask('templates', ['compileHtmlBars']);
    grunt.registerTask('styles', ['less']);
};