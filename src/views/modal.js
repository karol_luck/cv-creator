App.ModalView = Ember.View.extend({
    classNames: ['modal', 'fade'],
    layoutName: 'layouts/modal',
    title: 'Title',
    isModal: true,

    closeButton: 'Close',
    actionButton: false,
    actionName: null,

    actions: {
        doAction: function() {
            this.get('controller').send(this.get('actionName'));
            this.$().modal('hide');
        }
    }
});