App.LoginView = Ember.View.extend({
    templateName: 'views/login',
    logging: false,

    didInsertElement: function() {
        this._bindFormSubmit();
    },

    tryLogin: function(email, password) {
        var me = this,
            store = this.get('controller').store;
        this.set('logging', true);

        jQuery.post("/service/auth/login", {
            email: email,
            password: password
        }, function(response) {
            if(!response.id) {
                me.get('controller').send('alert', 'Bad credentials', 'danger');
                me.set('logging', false);
            }
            else {
                store.find('user', response.id).then(function(user) {
                    me.get('controller').set('user', user);
                    me.set('logging', false);
                });
            }
        }, "json");
    },

    _bindFormSubmit: function() {
        var me = this,
            form = this.$().find("form");
        form.on("submit", function(e) {
            me.tryLogin(form.find('.login-email').val(), form.find('.login-password').val());
            e.preventDefault();
        });
    }
});