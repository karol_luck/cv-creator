App.BlocksListView = Ember.View.extend({
    tagName: 'ul',
    classNames: ['list-group', 'blocks-list'],
    classNameBindings: ['isProject:project-list:not-project-list'],
    templateName: 'views/blocksList',

    list: null,
    isProject: false,
    canDelete: false,

    didInsertElement: function() {
        Ember.run.scheduleOnce('afterRender', this, '_afterRender');
    },

    _afterRender: function() {
        var me = this,
            controller = this.get('controller');

        this.$().sortable({
            items: "> li",
            tolerance: "pointer",
            connectWith: "ul." + ( this.get('isProject') ? 'blocks-list' : 'project-list' ),
            placeholder: "block-placeholder",

            stop: function(e, ui) {
                var item = ui.item,
                    index = ui.item.parent().children('li.block').index(item),
                    blockId = item.data('id'),
                    inProject = (item.hasClass('in-project')),
                    toProject = item.parent().hasClass('project-list');

                if(inProject) {
                    if(toProject) {
                        item.addClass('not-in-project');
                        controller.send('moveBlock', blockId, index);
                    }
                    else {
                        item.addClass('not-in-project');
                        controller.send('removeBlock', blockId);
                    }
                }
                else {
                    if(toProject) {
                        controller.send('addBlock', blockId, index);
                    }
                }

                return false;
            }
        });
    }
});