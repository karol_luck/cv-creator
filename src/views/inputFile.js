App.InputFileView = Ember.View.extend({
    tagName: 'button',
    classNames: ['btn', 'btn-default', 'file-uploader'],
    templateName: 'views/inputFile',

    didInsertElement: function() {
        this._bindOnChange();
    },

    getForm: function() {
        return this.$().find('form').get(0);
    },

    _bindOnChange: function() {
        var me = this;
        this.$().find('input').on('change', function() {
            me.get('controller').send('doUpload', me.get('name'), me.getForm());
        });
    }
});