App.HistoryEntryView = Ember.View.extend({
    templateName: 'views/historyEntry',
    classNames: ['history-entry'],

    entry: {},
    editing: false,
    newEntry: false,
    editor: null,

    listname: 'no-name',
    col1name: 'no-name',
    col2name: 'no-name',

    editingObserver: function() {
        if(!this.get('editor')) {
            Ember.run.scheduleOnce('afterRender', this, '_tinymceInit');
        }
    }.observes('editing'),

    didInsertElement: function() {
        Ember.run.scheduleOnce('afterRender', this, '_tinymceInit');
    },

    _tinymceInit: function() {
        var me = this,
            idSelector = "#" + this.$().attr('id'),
            selector = idSelector + " textarea.tinymce-enable";
        if(!$(selector).length) return;
        tinymce.init({
            selector: selector,
            menubar: false,
            forced_root_block : "",
            force_p_newlines : false,
            force_br_newlines : true,
            setup: function(editor) {
                var textarea = $(editor.getElement());
                editor.on('init', function() {
                    me.set('editor', editor);
                });
                editor.on('change', function() {
                    editor.save();
                    textarea.trigger('change');
                });
            }
        });
    },

    actions: {
        doRemove: function() {
            this.get('controller').removeFromList(this.get('listname'), this.get('entry'));
        },
        doEdit: function() {
            this.set('editing', true);
        },
        acceptEdit: function() {
            this.set('editing', false);
            this.get('controller').saveUser(this.get('listname'));
        }
    }
});