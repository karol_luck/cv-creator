App.User = DS.Model.extend({
    // Properties
    email: DS.attr('string'),
    name: DS.attr('string'),
    surname: DS.attr('string'),
    phone: DS.attr('string'),

    // Address properties
    street: DS.attr('string'),
    postcode: DS.attr('string'),
    city: DS.attr('string'),
    province: DS.attr('string'),
    country: DS.attr('string'),

    // About me properties
    //title: DS.attr('string'),
    //specialization: DS.attr('string'),
    birthday: DS.attr('string'),
    about: DS.attr('string'),

    abilities: DS.attr('array'),
    languages: DS.attr('array'),
    employment: DS.attr('userHistoryList'),
    education: DS.attr('userHistoryList'),

    timestamp: Date.now(),

    // Computed properties
    fullname: function() {
        return this.get('name') + ' ' + this.get('surname');
    }.property('name', 'surname'),
    avatarImage: function() {
        return '/service/file/avatarImage?userId=' + this.get('id') + '&timestamp=' + this.get('timestamp')
    }.property('id', 'timestamp'),

    documents: function() {
        if(!this.get('id')) return [];
        return this.store.find('document', { user: this.get('id') });
    }.property('id', 'timestamp'),

    // Custom methods
    changePassword: function(newPassword) {
        return new Ember.RSVP.Promise(function(resolve) {
            $.post("/service/auth/changePassword", {
                newPassword: newPassword
            }, function() {
                resolve();
            }, "json");
        });
    },
    reloadAvatar: function() {
        this.set('timestamp', Date.now());
    },
    generateBlocks: function(type) {
        var me = this;
        return new Ember.RSVP.Promise(function(resolve) {
            $.post("/service/blocks/generate", {
                type: type,
                user: me.get('id')
            }, function() {
                resolve();
            }, "json");
        });
    }
});

App.UserHistoryListTransform = DS.Transform.extend({
    deserialize: function(serialized) {
        return Ember.A(serialized);
    },
    serialize: function(deserialized) {
        return deserialized;
    }
});