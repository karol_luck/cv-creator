App.Document = DS.Model.extend({
    title: DS.attr('string'),
    user: DS.belongsTo('user'),
    blocks: DS.hasMany('block'),

    image: function() {
        return '/service/file/documentPreview?id=' + this.get('id');
    }.property('id'),

    generatePreview: function() {
        var me = this;
        return new Ember.RSVP.Promise(function(resolve) {
            $.post("/service/documents/generatePreview", {
                id: me.get('id')
            }, function () {
                resolve();
            }, "json");
        });
    }
});