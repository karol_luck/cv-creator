App.Block = DS.Model.extend({
    type: DS.attr('string'),
    title: DS.attr('string'),
    content: DS.attr('string'),
    value: DS.attr(),
    user: DS.belongsTo('user'),
    inProject: false,

    previewTimestamp: Date.now(),

    canEdit: function() {
        return this.get('type') == 'custom';
    }.property('type'),

    generate: function() {
        var me = this;
        return new Ember.RSVP.Promise(function(resolve) {
            jQuery.post("/service/blocks/generate", {
                type: me.get('type'),
                user: me.get('user.id'),
                id: me.get('id')
            }, function() {
                me.set('previewTimestamp', Date.now());
                resolve();
            }, "json");
        });
    },

    image: function() {
        return '/service/file/block?type=' + this.get('type') + (this.get('type') == 'custom' ? '&id=' + this.get('id') : '') + '&timestamp=' + this.get('previewTimestamp');
    }.property('user.id', 'type', 'id', 'previewTimestamp')
});