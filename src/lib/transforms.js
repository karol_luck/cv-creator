App.ArrayTransform = DS.Transform.extend({
    deserialize: function(serialized) {
        return Ember.A(serialized);
    },
    serialize: function(deserialized) {
        deserialized.sort();
        return deserialized.toArray();
    }
});

App.ObjectTransform = DS.Transform.extend({
    deserialize: function(serialized) {
        return Ember.Object(serialized);
    },
    serialize: function(deserialized) {
        return deserialized;
    }
});