var App = Ember.Application.create({});
App.ApplicationStore = DS.Store.extend({
});
App.ApplicationSerializer = DS.RESTSerializer.extend({
    primaryKey: '_id'
});
App.ApplicationAdapter = DS.RESTAdapter.extend({
    namespace: 'rest'
});