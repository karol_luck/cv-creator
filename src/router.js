App.Router.map(function() {
    this.route('login', { path: 'login' });
    this.route('profile', { path: 'profile' });
    this.route('experience', { path: 'experience' });
    this.route('creator', { path: 'creator' });
    this.route('creatorEdit', { path: 'editDocument/:docId'});
});

App.ApplicationRoute = Ember.Route.extend({
    actions: {
        alert: function(message, type, timeout) {
            this.controllerFor('application').send('showAlert', message, type, timeout);
        },
        setLoading: function(loading) {
            this.controllerFor('application').send('setLoading', loading);
        }
    },
    model: function() {
        var me = this,
            store = this.store;
        return new Ember.RSVP.Promise(function(resolve) {
            jQuery.getJSON("/service/auth/loggedUserId", function(json) {
                if(json.id) {
                    store.find('user', json.id).then(function(user) {
                        me.controllerFor('application').set('user', user);
                        resolve(user);
                    });
                }
                else {
                    resolve(null);
                }
            });
        });
    }
});

App.IndexRoute = Ember.Route.extend({
});

App.CreatorEditRoute = Ember.Route.extend({
    model: function(params) {
        return this.store.find('document', params.docId);
    },
    setupController: function(controller, model) {
        this.controllerFor('creator').set('loadedProject', model);
        this.transitionTo('creator');
    }
});

App.CreatorRoute = Ember.Route.extend({
    activate: function () {
        this.controllerFor('creator').activate();
    },
    model: function() {
        var userId = this.controllerFor('application').get('user.id');
        if(!userId) {
            this.transitionTo('/');
        }
        return this.store.find('block', {
            user: userId
        });
    },
    setupController: function(controller, blocks) {
        var basicBlocks = Ember.A(),
            customBlocks = Ember.A(),
            basicBlocksDict = {},
            bacisBlocksOrder = ['profile', 'profile2', 'employment', 'abilities', 'education', 'languages', 'footer'];

        blocks.forEach(function(block) {
            if(block.get('type') == 'custom') {
                customBlocks.push(block);
            }
            else {
                basicBlocksDict[block.get('type')] = block;
            }
        });

        bacisBlocksOrder.forEach(function(type) {
            if(basicBlocksDict[type]) {
                basicBlocks.push(basicBlocksDict[type]);
            }
        });

        if(controller.get('loadedProject')) {
            controller.set('project', controller.get('loadedProject'));
            controller.set('loadedProject', undefined);
        }
        else {
            controller.set('project', this.store.createRecord('document', {
                title: "New document",
                user: this.controllerFor('application').get('user')
            }));
        }
        controller.set('basicBlocks', basicBlocks);
        controller.set('customBlocks', customBlocks);
        controller._setInProjectBlocks();

        //console.log(controller.get('basicBlocks'), controller.get('customBlocks'));
    }
});

App.LoginRoute = Ember.Route.extend({
});