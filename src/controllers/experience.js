App.ExperienceController = Ember.Controller.extend({
    needs: 'application',
    user: Ember.computed.alias("controllers.application.user"),

    newEmployment: {},
    newEducation: {},

    saveUser: function(blockType) {
        var me = this,
            user = this.get('user');
        me.send('setLoading', true);
        user.save().then(function() {
            user.generateBlocks(blockType).then(function() {
                me.send('setLoading', false);
            });
        });
    },

    addToList: function(listName, value) {
        var list = this.get('user').get(listName);
        if(!list) {
            list = Ember.A();
            this.get('user').set(listName, list);
        }
        if(list.indexOf(value) == -1) {
            list.addObject(value);
            this.saveUser(listName);
        }
    },
    removeFromList: function(listName, value) {
        var list = this.get('user').get(listName);
        if(list) {
            list.removeObject(value);
            this.saveUser(listName);
        }
    },

    actions: {
        addAbility: function() {
            this.addToList('abilities', this.get('newAbility'));
            this.set('newAbility', undefined);
        },
        removeAbility: function(ability) {
            this.removeFromList('abilities', ability);
        },
        addLanguage: function() {
            this.addToList('languages', this.get('newLanguage'));
            this.set('newLanguage', undefined);
        },
        removeLanguage: function(language) {
            this.removeFromList('languages', language);
        },
        addEmployment: function() {
            this.addToList('employment', this.get('newEmployment'));
            this.set('newEmployment', {});
        },
        removeEmployment: function(employment) {
            this.removeFromList('employment', employment);
        },
        addEducation: function() {
            this.addToList('education', this.get('newEducation'));
            this.set('newEducation', {});
        },
        removeEducation: function(education) {
            this.removeFromList('education', education);
        }
    }
});