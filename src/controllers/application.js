App.ApplicationController = Ember.Controller.extend({
    user: null,

    alertType: 'alert-info',
    alertMessage: '---',
    isLoading: false,

    actions: {
        showAlert: function(message, type, timeout) {
            type = type || 'info';
            timeout = timeout || 2000;
            this.set('alertType', 'alert-' + type);
            this.set('alertMessage', message);
            $('#global-alert').fadeIn();
            Ember.run.later((function() {
                $('#global-alert').fadeOut();
            }), timeout);
        },
        setLoading: function(loading) {
            this.set('isLoading', loading);
        },
        logout: function() {
            var me = this;
            jQuery.getJSON('/service/auth/logout', function() {
                me.set('user', null);
            });
            this.transitionToRoute('index');
        }
    }
});