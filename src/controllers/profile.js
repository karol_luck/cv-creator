App.ProfileController = Ember.Controller.extend({
    needs: 'application',
    user: Ember.computed.alias("controllers.application.user"),

    newPassword: '',
    againPassword: '',
    avatarUploadValue: null,

    cantSavePassword: function() {
        var newPwd = this.get('newPassword'),
            againPwd = this.get('againPassword');

        if(newPwd.length <= 3 || againPwd.length <= 3) return true;

        return newPwd != againPwd;
    }.property('newPassword', 'againPassword'),

    actions: {
        saveUser: function() {
            var me = this;
            me.send('setLoading', true);
            this.get('user').save().then(function() {
                Ember.RSVP.all([
                    me.get('user').generateBlocks('profile'),
                    me.get('user').generateBlocks('profile2')
                ]).then(function() {
                    me.send('alert', 'Your profile has been saved successfully', 'success');
                    me.send('setLoading', false);
                });
            });
        },
        rollbackUser: function() {
            this.get('user').rollback();
        },
        clearNewPasswords: function() {
            this.set('newPassword', '');
            this.set('againPassword', '');
        },
        saveNewPassword: function() {
            var me = this;
            me.send('setLoading', true);
            this.get('user').changePassword(this.get('newPassword')).then(function() {
                me.send('clearNewPasswords');
                me.send('alert', 'Your password has been changed successfully', 'success');
                me.send('setLoading', false);
            });
        },
        deleteAvatar: function() {
            var me = this;
            this.send('setLoading', true);
            jQuery.getJSON('/service/file/avatarDelete', function() {
                me.send('alert', 'Your photo has been deleted', 'success');
                me.get('user').reloadAvatar();
                me.send('setLoading', false);
            });
        },
        doUpload: function(name, form) {
            if(name != 'avatarImage') {
                this.send('alert', 'Bad form name, expected: avatarUpload, get: ' + name, 'danger');
                return;
            }
            var me = this,
                formData = new FormData(form);
            this.send('setLoading', true);
            $.ajax({
                url: '/service/file/avatarUpload',
                type: 'POST',

                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).then(function(response) {
                var obj = jQuery.parseJSON(response);
                if(obj.status == 'ok') {
                    me.send('alert', 'Your photo has been updated', 'success');
                }
                else {
                    me.send('alert', obj.message, 'danger');
                }
                form.reset();
                me.get('user').reloadAvatar();
                me.send('setLoading', false);
            });
        }
    }
});