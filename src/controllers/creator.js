App.CreatorController = Ember.Controller.extend({
    needs: 'application',
    user: Ember.computed.alias("controllers.application.user"),

    basicBlocks: Ember.A(),
    customBlocks: Ember.A(),
    project: null,

    newCustomBlock: {},
    selectedCustomBlock: null,

    newEditor: null,
    selectedEditor: null,

    newObserver: function() {
        this._reloadEditor(this.get('newEditor'));
    }.observes('newCustomBlock'),
    selectedObserver: function() {
        this._reloadEditor(this.get('selectedEditor'));
    }.observes('selectedCustomBlock'),

    activate: function() {
        Ember.run.scheduleOnce('afterRender', this, this._bindTinyMce);
    },

    _setInProjectBlocks: function() {
        this.get('basicBlocks').forEach(function(block) {
            block.set('inProject', false);
        });
        this.get('customBlocks').forEach(function(block) {
            block.set('inProject', false);
        });
        this.get('project').get('blocks').forEach(function(block) {
            block.set('inProject', true);
        });
    },

    _clearItems: function() {
        $('.project-list').find('.not-in-project').remove();
    },

    _bindTinyMce: function() {
        var me = this;
        tinymce.init({
            selector: "textarea.tinymce-enabled",
            menubar: false,
            force_br_newlines: true,
            force_p_newlines: false,
            setup: function(editor) {
                var textarea = $(editor.getElement());
                editor.on('init', function() {
                    if(textarea.hasClass('new-editor')) {
                        me.set('newEditor', editor);
                    }
                    else if(textarea.hasClass('selected-editor')) {
                        me.set('selectedEditor', editor);
                    }
                });
                editor.on('change', function() {
                    editor.save();
                    textarea.trigger('change');
                });
            }
        });
    },

    _reloadEditor: function(editor) {
        Ember.run.scheduleOnce('afterRender', this, function() {
            editor.load();
        });
    },

    actions: {
        addBlock: function(id, index) {
            var project = this.get('project');
            this.store.find('block', id).then(function(block) {
                if (!block.get('inProject')) {
                    block.set('inProject', true);
                    if(index === undefined) {
                        project.get('blocks').pushObject(block);
                    } else {
                        project.get('blocks').insertAt(index, block);
                    }
                }
            });
        },
        moveBlock: function(id, index) {
            var me = this;
            this.store.find('block', id).then(function(block) {
                if(block.get('inProject')) {
                    me._clearItems();
                    me.get('project').get('blocks').removeObject(block);
                    me.get('project').get('blocks').insertAt(index, block);
                }
            });
        },
        removeBlock: function(id) {
            var me = this;
            var project = this.get('project');
            this.store.find('block', id).then(function(block) {
                project.get('blocks').removeObject(block);
                block.set('inProject', false);
                me._clearItems();
            });
        },

        addCustomBlock: function() {
            var me = this;
            var newRecord = this.store.createRecord('block', {
                title: this.get('newCustomBlock').title,
                content: this.get('newCustomBlock').content,
                type: 'custom',
                user: this.get('user')
            });
            this.send('setLoading', true);
            newRecord.save().then(function(savedRecord) {
                savedRecord.generate().then(function() {
                    me.customBlocks.addObject(savedRecord);
                    me.set('newCustomBlock', {});
                    me.send('alert', 'Block has been successfully added', 'success');
                    me.send('setLoading', false);
                });
            });
        },
        editCustomBlock: function() {
            var me = this;
            this.send('setLoading', true);
            this.get('selectedCustomBlock').save().then(function(savedBlock) {
                savedBlock.generate().then(function() {
                    me.send('alert', 'Block has been successfully saved', 'success');
                    me.send('setLoading', false);
                });
            });
        },
        toggleEditCustomBlock: function(block) {
            this.set('selectedCustomBlock', block);
            $('#edit-custom-block-modal').modal('show');
        },
        deleteCustomBlock: function(block) {
            var me = this;
            this.send('setLoading', true);
            this.get('user').get('documents').then(function(projects) {
                var saveProjects = [];
                projects.forEach(function(project) {
                    if(project.get('blocks').indexOf(block) >= 0) {
                        project.get('blocks').removeObject(block);
                        saveProjects.push(project.save());
                    }
                });
                Ember.RSVP.all(saveProjects).then(function() {
                    me.get('customBlocks').removeObject(block);
                    block.destroyRecord();
                    me.send('alert', 'Block has been successfully deleted', 'success');
                    me.send('setLoading', false);
                });
            });
        },

        saveProject: function() {
            var me = this,
                project = this.get('project');

            this.send('setLoading', true);
            project.save().then(function() {
                project.generatePreview().then(function() {
                    me.get('user').set('timestamp', Date.now());
                    me.send('setLoading', false);
                    me.send('alert', 'Document has been successfully saved', 'success');
                });
            });
        },
        printProject: function() {
            var form = $("#printForm");
            form.attr('action', '/service/documents/print');
            form.submit();
        },
        downloadProject: function() {
            var form = $("#printForm");
            form.attr('action', '/service/documents/download');
            form.submit();
        }
    }
});