App.IndexController = Ember.Controller.extend({
    needs: 'application',
    user: Ember.computed.alias("controllers.application.user"),

    documents: function() {
        if(!this.get('user')) return [];
        return this.get('user').get('documents');
    }.property('user', 'user.timestamp'),

    toRemove: null,

    actions: {
        editDocument: function(document) {
            this.transitionToRoute('creatorEdit', document.get('id'));
        },
        removeDocumentModal: function(document) {
            this.set('toRemove', document);
            $('#remove-document-modal').modal('show');
        },
        confirmRemoveDocument: function() {
            this.get('toRemove').destroyRecord();
            this.send('alert', 'Document has been successfuly deleted', 'success');
        }
    }
});