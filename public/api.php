<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

function __autoload($className) {
    if(file_exists("../backend/controllers/$className.php")) $classDir = 'controllers';
    else if((file_exists("../backend/classes/$className.php"))) $classDir = 'classes';
    else if((file_exists("../backend/models/$className.php"))) $classDir = 'models';
    else if((file_exists("../backend/interfaces/$className.php"))) $classDir = 'interfaces';
    else throw new Exception("Class file not exists ($className).");

    require_once("../backend/$classDir/$className.php");
}

if(!isset($_GET['controller'])) throw new Exception("Controller not set");
if($_GET['controller'] == 'rest') {
    $controllerName = 'RestController';
    if(!isset($_GET['method'])) throw new Exception("Method not set when using restController");
    $actionName = strtolower($_GET['method']).'Action';
}
else {
    $controllerName = ucfirst($_GET['controller']) . 'Controller';
    $action = $_GET['action'];
    $actionName = $action . 'Action';
}

$controller = new $controllerName();
$controller->$actionName();
